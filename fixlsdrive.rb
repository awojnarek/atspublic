#!/usr/bin/env ruby
require 'pp'

exit if ARGV[0] == nil 

bdarray = File.read(ARGV[0]).split("\n")
gdarray = []

# Build our good array from our bad one
bdarray.each do |line|

  if line == "id|status|error_sequence_number|use|tech_type|capacity|mdisk_id|mdisk_name|member_id|enclosure_id|slot_id|node_id|node_name|auto_manage"
    if gdarray.grep(line).empty?
      gdarray.push(line)
    end
  end 

  if line =~ /^[0-9]/ and line =~ /error_sequence_number/
    next
  end

  if line =~ /^[0-9]/ 
    if gdarray.grep(line).empty?
      gdarray.push(line)
    end
  end
end

# Let's make sure the new array is correct
if gdarray.grep("id|status|error_sequence_number|use|tech_type|capacity|mdisk_id|mdisk_name|member_id|enclosure_id|slot_id|node_id|node_name|auto_manage").empty?
  printf "No header found\n"
  exit 1
end

gdarray.each_cons(2) do |curr, nxt|
  next if curr =~ /^id/

  currid = curr.split("|")[0].to_i
  nextid = nxt.split("|")[0].to_i

  unless currid + 1 == nextid
    printf "Missing ID reference\n"
    exit 1
  end
end
  
# Let's write our shit out to a file now that we now it's good
File.open("lsdrive", "w+") do |f|
  f.puts(gdarray)
end
